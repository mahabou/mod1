/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: maabou-h <maabou-h@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/04 12:15:09 by maabou-h          #+#    #+#             */
/*   Updated: 2019/09/12 12:51:52 by maabou-h         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <GLUT/glut.h>
#include <iostream>

using namespace std;

int window;
GLfloat ctrlpoints[4][4][3] = {
	{{-1.0, 1.0, -1.0}, {-1.0, 1.0,-1.0 }, {1.0, 1.0, -1.0 }, {1.0, 1.0,-1.0}}, 
	{{-1.0, 1.0, -1.0}, {-1.0, 1.0,-1.0 }, {1.0, 1.0, -1.0 }, {1.0, 1.0,-0.0}}, 
	{{-1.0, 1.0,  1.0}, {-1.0, 1.0, 1.0 }, {1.0, 1.0,  1.0 }, {1.0, 1.0, 0.0}}, 
	{{-1.0, 1.0,  1.0}, {-1.0, 1.0, 1.0 }, {1.0, 1.0,  1.0 }, {1.0, 1.0, 1.0}}
};

void	display()
{
	int i, j;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glColor3f(1.0, 1.0, 1.0);
	glPushMatrix();
	glRotatef(50.0, 5.0, 5.0, 1.0);
	for (j = 0; j <= 16; j++) {
		glBegin(GL_LINE_STRIP);
		for (i = 0; i <= 30; i++)
			glEvalCoord2f((GLfloat)i/30.0, (GLfloat)j/16.0);
		glEnd();
		glBegin(GL_LINE_STRIP);
		for (i = 0; i <= 30; i++)
			glEvalCoord2f((GLfloat)j/16.0, (GLfloat)i/30.0);
		glEnd();
	}
	glPopMatrix();
	glFlush();
}

void	init()
{
	glClearColor(0.0, 0.0, 0.0, 0.0);
	glMap2f(GL_MAP2_VERTEX_3, 0, 1, 3, 4,
			0, 1, 12, 4, &ctrlpoints[0][0][0]);
	glEnable(GL_MAP2_VERTEX_3);
	glMapGrid2f(10.0, 0.0, 1.0, 10.0, 0.0, 1.0);
	glEnable(GL_DEPTH_TEST);
	glShadeModel(GL_FLAT);
}

void reshape(int w, int h)
{
	glViewport(0, 0, (GLsizei) w, (GLsizei) h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	if (w <= h)
		glOrtho(-5.0, 5.0, -5.0*(GLfloat)h/(GLfloat)w, 
				5.0*(GLfloat)h/(GLfloat)w, -5.0, 5.0);
	else
		glOrtho(-5.0*(GLfloat)w/(GLfloat)h, 
				5.0*(GLfloat)w/(GLfloat)h, -5.0, 5.0, -5.0, 5.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void	keyboard(unsigned char key, int x, int y)
{
	switch (key)
	{
		case 27:
			glutDestroyWindow(window);
			exit (0);
			break;
	}
	glutPostRedisplay();
}

int		main(int argc, char **argv) 
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize(800, 600);
	glutInitWindowPosition(100, 100);
	glutCreateWindow(argv[0]);
	init();
	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	glutKeyboardFunc(keyboard);
	glutMainLoop();
	return (0);
}
